//
//  GalleryViewController.swift
//  AnnaFlowerStudio
//
//  Created by Denis Lyakhovich on 01.05.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import UIKit
import Alamofire

class GalleryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, GalleryDataProviderDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var st: StudioModel?
    var dataProviderGallery: GalleryDataProvider?
    var refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: GalleryTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: GalleryTableViewCell.nibName)
        dataProviderGallery = GalleryDataProvider(d: self)
        dataProviderGallery?.loadGallery(index: st!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func  tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataProviderGallery!.gallery.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: GalleryTableViewCell.nibName) as! GalleryTableViewCell
        cell.castomize(gallery: (dataProviderGallery?.gallery[indexPath.row])!, studio: st!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 277
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func galleryDataLoaded() {
        tableView.reloadData()
    }
    
    func galleryDataHasError(error: String) {
        self.alertData()
    }
    
    public func alertData() {
        if dataProviderGallery!.gallery.count == 0 {
            let alert = UIAlertController(title: "Отсутствуют данные...",message: "Вернитесь обратно и подключитесь к сети",preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK",style: .default)
            alert.addAction(okAction)
            present(alert, animated: true)
        }
    }
}

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */




